# stage 1
FROM node:latest as node
WORKDIR /src/app
COPY . .
RUN npm install
RUN npm install -g @angular/cli
RUN npm run build --prod

# stage 2
FROM nginx:alpine
COPY --from=node /src/app/dist/ace /usr/share/nginx/html

EXPOSE 5000